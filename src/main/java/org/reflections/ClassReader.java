package org.reflections;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ClassReader {
    private static Scanner classInputScanner;

    public static void main(String[] args) {
        System.out.println("Please enter the class name");
        classInputScanner = new Scanner(System.in);
        doOperationsOnInputClass(classInputScanner.next());
    }

    private static void doOperationsOnInputClass(String className) {
        try {
            Class classToRead = Class.forName(className);
            boolean continueExecution = true;
            do {
                System.out.println("Select the Menu option");
                System.out.println("1. Methods");
                System.out.println("2. Class");
                System.out.println("3. Subclasses");
                System.out.println("4. Parent Classes");
                System.out.println("5. Constructors");
                System.out.println("6. Data Members");
                int option = classInputScanner.nextInt();
                switch (option) {
                    case 1:
                        Method[] methods = classToRead.getMethods();
                        List<String> methodList = new ArrayList<>();
                        for (Method method : methods) {
                            methodList.add(method.getName());
                            System.out.println(method);
                        }
                        continueExecution = isContinueExecution(classToRead, continueExecution, methodList, "fetching methods");
                        break;
                    case 2:
                        System.out.println(classToRead.getName());
                        break;
                    case 3:
                        Reflections ref = new Reflections("org.reflections");
                        Set<Class<? extends Object>> subTypes = ref.getSubTypesOf(classToRead);
                        List<String> subClassList = new ArrayList<>();
                        for (Class subClass : subTypes) {
                            subClassList.add(subClass.getName());
                            System.out.println(subClass.getName());
                        }
                        continueExecution = isContinueExecution(classToRead, continueExecution, subClassList, "fetching sub classes");
                        System.out.println("There is no way to find subclasses of a class");
                        break;
                    case 4:
                        Class parentClass = classToRead.getSuperclass();
                        List<String> parentClassList = new ArrayList<>();
                        parentClassList.add(parentClass.getName());
                        System.out.println("Parent class: " + parentClass.getName());
                        continueExecution = isContinueExecution(classToRead, continueExecution, parentClassList, "fetching parent class");
                        break;
                    case 5:
                        Constructor[] constructors = classToRead.getConstructors();
                        List<String> constructorList = new ArrayList<>();
                        for (int i = 0; i < constructors.length; i++) {
                            constructorList.add(constructors[i].toString());
                            System.out.println(constructors[i]);
                        }
                        continueExecution = isContinueExecution(classToRead, continueExecution, constructorList, "fetching constructors");
                        break;
                    case 6:
                        Field[] dataMembers = classToRead.getDeclaredFields();
                        List<String> dataMembersList = new ArrayList<String>();
                        for (Field field : dataMembers) {
                            dataMembersList.add(field.getName());
                            System.out.println(field.getName());
                        }
                        continueExecution = isContinueExecution(classToRead, continueExecution, dataMembersList, "fetching data members");
                        break;
                    default:
                        continueExecution = false;
                        break;
                }
            } while (continueExecution);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean isContinueExecution(Class classToRead, boolean continueExecution, List<String> list, String operation) throws IOException {
        if (!list.isEmpty()) {
            continueExecution = checkOtherInformation(list, classToRead, operation);
        }
        return continueExecution;
    }

    private static boolean checkOtherInformation(List<String> operationList, Class classToRead, String operationName) throws IOException {
        System.out.println("Do you want to see any other information");
        System.out.println("Press yes to recheck the menu and no if you want to continue");
        String continueChecking = classInputScanner.next();
        if (continueChecking.equals("no")) {
            boolean continueExecution = true;
            do {
                System.out.println("1. Store information into file");
                System.out.println("2. To see all previous files created");
                System.out.println("3. Exit without storing");
                int fileOperation = classInputScanner.nextInt();
                switch (fileOperation) {
                    case 1:
                        org.reflections.FileOperations.writeIntoFile(operationList, classToRead, operationName);
                        System.out.println("File write done");
                        break;
                    case 2:
                        org.reflections.FileOperations.readFromFile();
                        break;
                    case 3:
                        continueExecution = false;
                        break;
                }
            } while (continueExecution);
            return true;
        } else if (continueChecking.equals("yes")) {
            return true;
        }
        return false;

    }
}
