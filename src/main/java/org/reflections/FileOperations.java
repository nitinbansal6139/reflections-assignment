package org.reflections;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class FileOperations {

    public static final String E_FILES = "E://files";

    public static void writeIntoFile(List<String> operationList, Class classToRead, String operationName) throws IOException {
        try (FileWriter writer = new FileWriter(E_FILES + "//record_" + classToRead.getName() + "_" + new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()) + ".txt")) {
            writer.write(classToRead.getName() + "->" + operationName + "\n");
            for (String text : operationList) {
                writer.write(text + "\n");
            }
        }
    }

    public static void readFromFile() throws IOException {
        File file = new File(E_FILES);
        File[] files = file.listFiles();
        Arrays.sort(files);
        for (File createdFile : files) {
            try (BufferedReader br = new BufferedReader(new FileReader(createdFile))) {
                String str;
                System.out.println(createdFile.getName());
                while ((str = br.readLine()) != null)
                    System.out.println(str);
            }
        }
    }
}
